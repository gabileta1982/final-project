#!/bin/bash

# Persistent Volume
microk8s.kubectl create -f kubernetes/final-pv.yaml

# PVClaim
microk8s.kubectl create -f kubernetes/final-pvc.yaml

# Deployment
microk8s.kubectl create -f kubernetes/final-deploy.yaml

# Service
microk8s.kubectl create -f kubernetes/final-service.yaml

# Images
sleep 5
sudo cp images/*.jpeg /tmp/images/