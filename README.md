# final-project

## Proyecto final de actualización tecnologica.

> Este repositorio contiene los archivos necesarios para poder crear un cluster de kubernetes, levantar una imagen de docker y exponerla.
> La imagen, contiene una página de contenido estático en apache, con fotos, videos y links con información sobre los basset hounds.

---
[Dcoumentación Microk8s](https://microk8s.io/docs)

### Instalamos el gestor de paquetes Snap
```
sudo apt install snapd
```
### Instalación Microk8s
```
sudo snap install microk8s --classic --channel=1.21
```

### Configuramos permisos para no usar root
```
sudo usermod -a -G microk8s $USER
sudo chown -f -R $USER ~/.kube
```
### Nos relogueamos o corremos el siguiente comando
```
microk8s status --wait-ready
```
### Listar nodos e información del cluster
```
microk8s.kubectl get nodes -o wide
```
---

## Despliegue Kubernetes
###  Corriendo el siguiente script podemos implementar un volumen persistente, el reclamo del mismo, un deployment con sus respectivos pods y replica sets, y el servicio para exponer la aplicación usando NodePort.

```
./deploy.sh
```
### Verificamos que se hayan desplegado de forma correcta todos los componentes
```
microk8s.kubectl get all
```

### Podemos también ver el puerto expuesto por NodePort
```
microk8s.kubectl get service -o wide
```

### La URL para acceder la página
http://localhost:30741/


## Docker
### Imagen utilizada
https://hub.docker.com/repository/docker/gabileta1982/basset-hound

#### Dentro del repositorio en el directorio "docker", podremos encontrar los archivos usados para armar la imagen subida a docker hub.

## Borrado
Para poder borrar los componentes implementados ejecutamos el siguiente script

```
./destroy.sh
```

### Para poder para el cluster corremos
```
microk8s stop
```