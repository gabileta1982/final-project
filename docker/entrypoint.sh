#!/bin/bash
set -e

# Vaciamos directorio /var/www/html
if [ -f /var/www/html/index.html ]; then
rm -fr /var/www/html/index.html
fi

# Modificar los valores del fichero default.conf 'ServerAlias' y 'ServerName'
sed -i 's/APPSERVERNAME/'"$APPSERVERNAME"'/' /etc/apache2/sites-available/default.conf
sed -i 's/APPALIAS/'"$APPALIAS"'/' /etc/apache2/sites-available/default.conf

cat <<EOF > /var/www/html/index.html
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<title>Le Basset Hound</title>
</head>

<style>
body {
  background-image: url('images/basset-avengers.jpeg');
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-size: 100% 100%;
}
</style>

<body style="color:DodgerBlue;text-align:center;">

<h1>
&emsp;
&emsp;
&emsp;

Batatas

&emsp;
&emsp;
&emsp;
</h1>

<section>
<iframe width="560" height="315" src="https://www.youtube.com/embed/Tp9WChcjPB4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</section>

</body>
&emsp;

<section>
<img src="images/gabi-otto.jpeg">
</section>

<p>
El basset hound es una raza de perro que forma parte del grupo de los sabuesos. Es de baja altura de cruz, cuerpo largo, patas cortas y fuertes, largas orejas y un sentido del olfato extremadamente desarrollado. Generalmente se le usa para la caza de venados, liebres y zorros, entre otros animales como el faisán, ya que su caza es más especializada. El origen de esta raza data de finales del siglo XIX, cuando un perro inglés fue cruzado con un blood hound.

Debido a su peculiar aspecto físico, es una raza popular y fácilmente reconocible para todo tipo de aficionados canófilos e incluso para personas con poco conocimiento de razas caninas, por lo que reúne muchos admiradores y aficionados a su cría, fundamentalmente en los países de origen anglosajón, en los que tiene una gran aceptación.
</p>

<section>
<a href= "https://basset-bhca.org/">Official Site</a>
</section>

EOF

chmod 755 /var/www/html/index.html

# Activar el sitio
a2ensite default.conf

# Inicio de servicio
apachectl -DFOREGROUND
service apache2 restart
exec "$@"