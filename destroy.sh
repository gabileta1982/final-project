#!/bin/bash

# Service
microk8s.kubectl delete service $(microk8s.kubectl get svc | grep final | awk '{print $1}')

# Deploy
microk8s.kubectl delete deploy $(microk8s.kubectl get deploy | grep final | awk '{print $1}')
sleep 5

# Images
sudo rm -fr /tmp/images/*.jpeg

# PVClaim
microk8s.kubectl delete pvc $(microk8s.kubectl get pvc | grep final | awk '{print $1}')
sleep 10

# PV
microk8s.kubectl delete pv $(microk8s.kubectl get pv | grep final | awk '{print $1}')